package cl.iazoc.juegosy2a.Entidades;

import java.util.ArrayList;

/**
 * Created by iazoc on 23/06/2017.
 */

public class Juego {

    private int id,precio, stock;
    private String nombre,genero, consola,  idioma, descripcion, fechaLanzamiento, url;
    private ArrayList<Comentario> comentarios;

    public Juego() {
    }

    public Juego(String nombre) {this.id = id;
        this.nombre = nombre;
   }

    public Juego(String nombre, int precio) {this.id = id;
        this.nombre = nombre;
        this.precio = precio;
    }

    public Juego(String nombre, int precio, String url) {
        this.nombre = nombre;
        this.precio = precio;
        this.url = url;
    }

    public Juego(int id, String nombre, String genero, String consola, int precio, String idioma, int stock, String descripcion, String fechaLanzamiento, String url) {
        this.id = id;
        this.nombre = nombre;
        this.genero = genero;
        this.consola = consola;
        this.precio = precio;
        this.idioma = idioma;
        this.stock = stock;
        this.descripcion = descripcion;
        this.fechaLanzamiento = fechaLanzamiento;
        this.url = url;
    }

    public Juego(int id, String nombre, String genero, String consola, int precio, String idioma, int stock, String descripcion, String fechaLanzamiento, String url, ArrayList<Comentario> comentarios) {
        this.id = id;
        this.nombre = nombre;
        this.genero = genero;
        this.consola = consola;
        this.precio = precio;
        this.idioma = idioma;
        this.stock = stock;
        this.descripcion = descripcion;
        this.fechaLanzamiento = fechaLanzamiento;
        this.url = url;
        this.comentarios = comentarios;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getConsola() {
        return consola;
    }

    public void setConsola(String consola) {
        this.consola = consola;
    }


    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaLanzamiento() { return fechaLanzamiento; }

    public void setFechaLanzamiento(String fechaLanzamiento) { this.fechaLanzamiento = fechaLanzamiento; }

    public String getUrl() { return url; }

    public void setUrl(String url) { this.url = url; }

    public ArrayList<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(ArrayList<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

}
