package cl.iazoc.juegosy2a.Entidades;

/**
 * Created by iazoc on 23/06/2017.
 */

public class Comentario {

    private int id;
    private String comentario;
    private double valoracion;
    private Usuario _usuario;
    private Juego _juego;

    public Comentario() {
    }

    public Comentario(int id, String comentario, double valoracion, Usuario _usuario, Juego _juego) {
        this.id = id;
        this.comentario = comentario;
        this.valoracion = valoracion;
        this._usuario = _usuario;
        this._juego = _juego;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public double getValoracion() {
        return valoracion;
    }

    public void setValoracion(double valoracion) {
        this.valoracion = valoracion;
    }

    public Usuario get_usuario() {
        return _usuario;
    }

    public void set_usuario(Usuario _usuario) {
        this._usuario = _usuario;
    }

    public Juego get_juego() {
        return _juego;
    }

    public void set_juego(Juego _juego) {
        this._juego = _juego;
    }

}
