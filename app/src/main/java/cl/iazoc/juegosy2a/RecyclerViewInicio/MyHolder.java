package cl.iazoc.juegosy2a.RecyclerViewInicio;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import cl.iazoc.juegosy2a.Entidades.Juego;
import cl.iazoc.juegosy2a.R;

/**
 * Created by iazoc on 23/06/2017.
 */

public class MyHolder extends RecyclerView.ViewHolder {

    TextView tvNombre, tvPrecio;
    ImageView ivJuego;
    private List<Juego> juegos;

    public MyHolder(final View itemView, final List<Juego> juegos) {
        super(itemView);
        this.juegos = juegos;
        ivJuego = (ImageView) itemView.findViewById(R.id.ivJuego);
        tvNombre = (TextView) itemView.findViewById(R.id.tvNombre);
        tvPrecio = (TextView) itemView.findViewById(R.id.tvPrecio);

        ivJuego.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "nombre: "+tvNombre.getText().toString() + " / precio: " + tvPrecio.getText().toString() , Toast.LENGTH_SHORT).show();
            }
        });

    }

}
