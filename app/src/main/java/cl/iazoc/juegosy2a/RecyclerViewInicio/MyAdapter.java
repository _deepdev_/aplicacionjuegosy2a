package cl.iazoc.juegosy2a.RecyclerViewInicio;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import cl.iazoc.juegosy2a.Entidades.Juego;
import cl.iazoc.juegosy2a.R;

/**
 * Created by iazoc on 23/06/2017.
 */

public class MyAdapter extends RecyclerView.Adapter<MyHolder> {

    Context c;
    ArrayList<Juego> juegos;

    public MyAdapter(Context c, ArrayList<Juego> juegos) {
        this.c = c;
        this.juegos = juegos;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        MyHolder viewHolder = null;
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.juegos_items,parent,false);
        viewHolder=new MyHolder(v, juegos);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        Picasso.with(c).load(juegos.get(position).getUrl()).placeholder(holder.ivJuego.getDrawable()).fit().into(holder.ivJuego);
        holder.tvNombre.setText(juegos.get(position).getNombre()+"");
        holder.tvPrecio.setText(juegos.get(position).getPrecio()+"");
    }

    @Override
    public int getItemCount() {
        return this.juegos.size();
    }
}
