package cl.iazoc.juegosy2a;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.facebook.login.LoginManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import android.support.v7.widget.Toolbar;
import android.widget.TabHost;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import cl.iazoc.juegosy2a.Entidades.Juego;
import cl.iazoc.juegosy2a.RecyclerViewInicio.MyAdapter;

public class Inicio extends AppCompatActivity {

    private RecyclerView rv;
    private LinearLayoutManager linearLayoutManager;
    private MyAdapter adapter;
    private DatabaseReference databaseReference;
    private ArrayList<Juego> juegos;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;
    private Button btnLogout;
    private GoogleApiClient googleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        /**TOOLBAR*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Y2A");
        setSupportActionBar(toolbar);

        /**TABLAS*/
        Resources res = getResources();

        TabHost tabs=(TabHost)findViewById(android.R.id.tabhost);
        tabs.setup();

        /**TABLA UNO LISTA TODOS LOS JUEGOS*/
        TabHost.TabSpec spec=tabs.newTabSpec("mitab1");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Todos los Juegos",
                res.getDrawable(android.R.drawable.ic_menu_gallery));
        tabs.addTab(spec);

        /**--------------------------*/

        /**TABLA DOS FILTRO POR GENERO*/
        spec=tabs.newTabSpec("mitab2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Filtro por Género",
                res.getDrawable(android.R.drawable.ic_dialog_map));
        tabs.addTab(spec);

        tabs.setCurrentTab(0);

        /**--------------------------------*/

        /**TABLA TRES FILTRO POR PRECIO*/

        spec=tabs.newTabSpec("mitab3");
        spec.setContent(R.id.tab3);
        spec.setIndicator("Filtro por Precio",
                res.getDrawable(android.R.drawable.ic_dialog_map));
        tabs.addTab(spec);

        tabs.setCurrentTab(0);
        /**------------------------------*/


        /**RECYCLER VIEW*/
        juegos = new ArrayList<Juego>();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        rv = (RecyclerView) findViewById(R.id.mRecylcerID);
        linearLayoutManager = new GridLayoutManager(this,3);
        rv.setLayoutManager(linearLayoutManager);
        rv.hasFixedSize();
        btnLogout = (Button)findViewById(R.id.btnLogout);


        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout(v);
            }
        });

        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                getAllJuegos(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                getAllJuegos(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });

    }

    private void getAllJuegos(DataSnapshot dataSnapshot){
        juegos.clear();

        for(DataSnapshot singleSnapshot : dataSnapshot.getChildren()){
            String url = singleSnapshot.getValue(Juego.class).getUrl();
            String nombre = singleSnapshot.getValue(Juego.class).getNombre();
            int precio = singleSnapshot.getValue(Juego.class).getPrecio();
            juegos.add(new Juego(nombre, precio, url));
            adapter = new MyAdapter(Inicio.this, juegos);
            rv.setAdapter(adapter);
        }

    }

    private void goLoginScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

 public void logout(View view) {
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        goLoginScreen();
    }   /*
    private void goLogInScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void Logout() {

        firebaseAuth.signOut();

        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if (status.isSuccess()) {
                    goLoginScreen();
                } else {
                    Toast.makeText(getApplicationContext(), "no se pudo iniciar sesion", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }*/

}
